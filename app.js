let initDb = require('./initDb.js'),
  express = require('express'),
  low = require('lowdb'),
  shortid = require('shortid'),
  FileSync = require('lowdb/adapters/FileSync'),
  adapter = new FileSync('./db/db.json'),
  db = low(adapter),
  app = express(),
  lastTimeChecked,
  results,
  sslChecker = require('ssl-checker'),
  path = require('path'),
  SlackWebhook = require('slack-webhook'),
  slack52DN = new SlackWebhook('https://hooks.slack.com/services/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', {
    defaults: {
      username: 'CertChecker',
      icon_emoji: ':robot_face:'
    }
  }),
  slackBert = new SlackWebhook('https://hooks.slack.com/services/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', {
    defaults: {
      username: 'CertChecker',
      icon_emoji: ':robot_face:'
    }
  });



async function checkAllCerts() {
  results = {ssl:[],other:[]};
  db.get('urls');
  var sites = db.value().urls;
  for (var i = 0; i < sites.length; i++) {
    try {
      checkSSLCert(sites[i])
    } catch (err) {
      console.log(err);
      results.ssl.push({
        id: sites[i].id,
        host: sites[i].url,
        slack: sites[i].slack,
        daysLeft: 0,
        expiryDate: 'error'
      });
    }
  }
  var otherCerts = db.value().other;
  for (var j = 0; j < otherCerts.length; j++) {
    try {
      checkOtherCert(otherCerts[j])
    }
    catch (err) {
      results.other.push({
        id:otherCerts[j].id,
        name:otherCerts[j].name,
        slack:otherCerts[j].slack,
        expiryDate:'error',
        daysLeft:0
      })
    }
  }
  lastTimeChecked = { ts: Date.now() };
}

async function checkSSLCert(site) {
  console.log(site)
  await sslChecker(site.url, 'GET', 443)
    .then(function(result) {
      results.ssl.push({
        id: site.id,
        host: site.url,
        slack: site.slack,
        daysLeft: result.days_remaining,
        expiryDate: result.valid_to
      });
      console.log(result);
      return result;
    })
    .then(function(result) {
      if (result.days_remaining == 30 || result.days_remaining == 14 || result.days_remaining == 7) {
        var text = site.url + ' SLL certificate expires in ' + result.days_remaining + ' days';
        if (site.slack === '52dn') {
          slack52DN.send({
            text: text
          });
        } else {
          slackBert.send({
            text: text
          });
        }
      }
    })
    .catch(function(err) {
      console.log(err);
      results.ssl.push({
        id: site.id,
        host: site.url,
        slack: site.slack,
        daysLeft: 0,
        expiryDate: 'error'
      });
    });
}

async function checkOtherCert(cert) {
  console.log(cert)
  try {
    var daysLeft = Math.round(((cert.expiryDate - new Date())) / 1000 / 60 / 60 / 24);
    console.log(daysLeft);
    console.log(new Date(cert.expiryDate).getDate(), new Date().getDate())
    results.other.push({
      id: cert.id,
      name: cert.name,
      slack: cert.slack,
      expiryDate: new Date(cert.expiryDate).toLocaleString(),
      daysLeft: daysLeft
    });
    if (daysLeft == 160 || daysLeft == 14 || daysLeft == 7) {
      var text = cert.name + ' Certificate expires in ' + daysLeft + ' days ';
      if (cert.slack === '52dn') {
        slack52DN.send({
          text: text
        });
      } else {
        slackBert.send({
          text: text
        });
      }
    }
  }
  catch (err){
    results.other.push({
      id:cert.id,
      name:cert.name,
      slack:cert.slack,
      expiryDate:'error1',
      daysLeft:0
    })
  }
}


/*API CALLS*/
app.get('/api', function(req, res) {
  var sortedSSLResult = results.ssl.sort(function(a, b) {
    return a.daysLeft - b.daysLeft;
  });
  var sortedOtherResult = results.other.sort(function(a, b) {
    return a.daysLeft - b.daysLeft;
  });
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.send({
    results: {
      ssl:sortedSSLResult,
      other:sortedOtherResult,
    },
    lastTimeChecked: lastTimeChecked,
    api: ['/api/', '/api/addUrl', '/api/deleteUrl']
  });
});
app.get('/api/addUrl', function(req, res) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  var url = req.query.url
      .replace('https://', '')
      .replace('http://', '')
      .replace('www.', '')
      .split('/')[0],
    slack = req.query.slack,
    id = shortid.generate();
  db.get('urls')
    .push({
      id: id,
      slack: slack,
      url: url
    })
    .write();
    checkSSLCert({id: id,
      slack: slack,
      url: url
    });
  res.send({
    added: {
      id: id,
      url: url
    }
  });
});
app.get('/api/addCert', function(req, res) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  var rawSplittedExpiryDate = req.query.expiryDate.split('/'),
    certObject = {
    id: shortid.generate(),
    expiryDate:new Date(rawSplittedExpiryDate[2],parseInt(rawSplittedExpiryDate[0])-1,rawSplittedExpiryDate[1]).getTime(),
    slack: req.query.slack,
    name: req.query.name
  };
  db.get('other')
    .push(certObject)
    .write();
  checkOtherCert(certObject);
  res.send(certObject);
});
app.get('/api/deleteUrl', function(req, res) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  var id = req.query.id;
  db.get('urls')
    .remove({ id: id })
    .write();
  for (var i = 0; i < results.ssl.length; i++) {
    if(results.ssl[i].id == id){
      results.ssl.splice(i,1)
    }
  }
  res.send({ removed: id });
});
app.get('/api/deleteCert', function(req, res) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  var id = req.query.id;
  db.get('other')
    .remove({ id: id })
    .write();
  for (var i = 0; i < results.other.length; i++) {
    if(results.other[i].id == id){
      results.other.splice(i,1)
    }
  }
  res.send({ removed: id });
});
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
});
app.get('/index.js', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.js'));
});
app.get('/custom.css', function(req, res) {
  res.sendFile(path.join(__dirname + '/custom.css'));
});
app.get('/db/db.json', function(req, res) {
  res.sendFile(path.join(__dirname + '/db/db.json'));
});

app.listen(8080);

/*START*/
setInterval(function() {
  checkAllCerts();
}, 86400000);
initDb.initDB(db);
if(db.get('other').value() === undefined){
  db.set('other', [])
    .write();
}

checkAllCerts();
