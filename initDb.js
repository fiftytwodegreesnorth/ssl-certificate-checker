/**
 * Created by jesper on 26/09/2018.
 */
var shortid = require('shortid');
module.exports = {
  initDB: function(db) {
    db.defaults({
      urls: [
        {
          "id": "vVvdIUaRFX",
          "url": "weare52dn.com",
          "slack": "52dn"
        },
        {
          "id": "vyaAQOXVA",
          "url": "grip52.com",
          "slack": "52dn"
        },
        {
          "id": "ivNnUbmt7T",
          "url": "ursulineweg.nl",
          "slack": "bert"
        },
        {
          "id": "U_6M5kils",
          "url": "kaboodle.nl",
          "slack": "bert"
        },
        {
          "id": "BRnmyGRR0",
          "url": "myosotes.com",
          "slack": "52dn"
        },
        {
          "id": "9Zt5S30ijh",
          "url": "kroesewevers.nl",
          "slack": "bert"
        },
        {
          "id": "doK65x1sT",
          "url": "events.kroesewevers.nl",
          "slack": "bert"
        },
        {
          "id": "g7ntaFbuJ",
          "url": "jb-test.52dn.nl",
          "slack": "52dn"
        },
        {
          "id": "V0Yzlnz8y8",
          "url": "ipsumenergy.com",
          "slack": "52dn"
        },
        {
          "id": "tmEn2kj1J",
          "url": "kromm.nl",
          "slack": "bert"
        },
        {
          "id": "PmOBKYAvI",
          "url": "hetkangewoon.nl",
          "slack": "52dn"
        },
        {
          "id": "jxhN_nBqK",
          "url": "eu.aoc.com",
          "slack": "bert"
        },
        {
          "id": "x98Jcg9iMY",
          "url": "ekt-digital.com",
          "slack": "bert"
        },
        {
          "id": "-3Q1u5US0",
          "url": "veiligvakwerk.nl",
          "slack": "52dn"
        },
        {
          "id": "VnYX6_Q2m",
          "url": "portal.veiligvakwerk.nl",
          "slack": "52dn"
        },
        {
          "id": "KQ-hiCbBC",
          "slack": "bert",
          "url": "wearebert.com"
        }
      ],
      other: []
    }).write();
  }
}