FROM node:9
WORKDIR /app
VOLUME /app/db
COPY package*.json ./

RUN npm install
COPY . .
EXPOSE 8080
CMD [ "npm", "start" ]
