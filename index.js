loadData = function () {
  $("tbody").empty()
  $.get("/api", function (data) {
    var color;
    if (data.results.ssl === undefined) {
      $("#table").append('<tr>' +
        '<td>No data retrieved</td>' +
        '<td>reloading in 3 seconds....</td>' +
        '</tr>');
      setTimeout(function () {
        location.reload()
      }, 3000)
    }
    data.results.ssl.forEach(function (entry) {
      color = "";
      if (entry.daysLeft < 30) {
        color = "warning"
      }
      if (entry.daysLeft < 7) {
        color = "danger"
      }
      var url = entry.host;
      var id = entry.id;
      $("#sslTable").append('<tr>' +
        '<td>' + entry.host + '</td>' +
        '<td>' + entry.expiryDate + '</td>' +
        '<td>' + entry.slack + '</td>' +
        '<td class="text-' + color + ' text-center">' + entry.daysLeft + '</td>' +
        '<td class="text-center" >' +
        '<span onclick="removeUrlButtonHandler(this)" id="' + id + '"><i class="fas fa-times"></i></span>' +
        '</td>' +
        '</tr>')
    });
    data.results.other.forEach(function (entry) {
      color = "";
      if (entry.daysLeft < 30) {
        color = "warning"
      }
      if (entry.daysLeft < 7) {
        color = "danger"
      }
      var url = entry.host;
      var id = entry.id;
      $("#otherTable").append('<tr>' +
        '<td>' + entry.name + '</td>' +
        '<td>' + entry.expiryDate + '</td>' +
        '<td>' + entry.slack + '</td>' +
        '<td class="text-' + color + ' text-center">' + entry.daysLeft + '</td>' +
        '<td class="text-center" >' +
        '<span onclick="removeCertButtonHandler(this)" id="' + id + '"><i class="fas fa-times"></i></span>' +
        '</td>' +
        '</tr>')
    });
    console.log(data.lastTimeChecked.ts);
    $("#lastTimeChecked").text(new Date(data.lastTimeChecked.ts).toLocaleString());
  });
};
$("#addCertButton").on('click',function () {
  $('.datepicker').datepicker()
});
$("#otherNavButton").on('click',function () {
  $("#addCertButton").attr('data-target','#addCertModal').text('Add Certificate')
});
$("#sslNavButton").on('click',function () {
  $("#addCertButton").attr('data-target','#addUrlModal').text('Add SSL Certificate')
});

addUrlButtonHandler = function (el) {
  $.get("/api/addUrl?url=" + $("#newUrlInput").val() + "&slack="+ $("#notifySlack").val(), function (data) {
    console.log(data)
  });
  $(el).html('<i class="fas fa-spinner fa-spin"></i>');
  setTimeout(
    function () {
      loadData();
      $('#addUrlModal').modal('hide');
      $(el).html('Add Url');
    }, 800)
};
addCertButtonHandler = function (el) {
  $.get("/api/addCert?name=" + $("#newCertInput").val() + "&slack="+ $("#notifySlack").val()+ "&expiryDate="+ $("#newCertExpiryDate").val(), function (data) {
    console.log(data)
  });
  $(el).html('<i class="fas fa-spinner fa-spin"></i>');
  setTimeout(
    function () {
      loadData();
      $('#addUrlModal').modal('hide');
      $(el).html('Add Url');
    }, 800)
};
removeUrlButtonHandler = function (el) {
  var id = $(el).attr('id');
  $(el).html('<i class="fas fa-spinner fa-spin"></i>');
  $.get("/api/deleteUrl?id=" + id + "", function (data) {
    console.log(data)
  });
  setTimeout(
    function () {
      loadData();
    }, 800)
};
removeCertButtonHandler = function (el) {
  var id = $(el).attr('id');
  $(el).html('<i class="fas fa-spinner fa-spin"></i>');
  $.get("/api/deleteCert?id=" + id + "", function (data) {
    console.log(data)
  });
  setTimeout(
    function () {
      loadData();
    }, 800)
};
loadData();